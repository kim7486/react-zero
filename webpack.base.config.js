const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); // 生成 html 模板
const CleanWebpackPlugin = require('clean-webpack-plugin'); // 构建前清理指定文件

module.exports = {
  entry: {
    index: path.resolve(__dirname, 'src/app.js'),
    vendor: ['react', 'react-dom'],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: '[name].js', // 通过动态导入进行 chunk 分离
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      }, {
        test: /\.scss$/,
        include: /\.src/,
        loader: 'style!css!sass?sourceMap=true&sourceMapContents=true',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'React-zero', // 好像没啥用
      template: path.resolve(__dirname, 'src/views/index.html'),
    }),
    new CleanWebpackPlugin(['./dist']),
  ],
  resolve: {
    // 解析模块请求
    extensions: ['.js', '.json', '.jsx', '.scss', '.css'],
    alias: {

    },
  },
};
