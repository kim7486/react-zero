const merge = require('webpack-merge');
const webpack = require('webpack');
const config = require('./webpack.base.config');

module.exports = merge(config, {
  mode: 'development',
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      },
    }),
  ],
});
